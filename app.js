var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var zmq = require('zmq');
var nomo = require('node-monkey').start({silent: true});
var bunyan = require('bunyan');

var app = express();

var log = bunyan.createLogger({
  name   : 'app',
  streams: [
    {
      level : 'info',
      stream: nomo.stream
    }
  ]
});

var zmqAddress = 'tcp://127.0.0.1:3001';

var pub = zmq.socket('pub');
pub.bindSync(zmqAddress);

app.set('pub', pub);
app.set('log', log);

var sock = zmq.socket('sub');
sock.connect(zmqAddress);
sock.subscribe('channel1');

sock.on('message', function(topic, message){
  log.info(topic.toString(), message.toString());
});

var routes = require('./routes/index');
var users = require('./routes/users');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
