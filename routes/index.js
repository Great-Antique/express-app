var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var pub = req.app.get('pub');
  var log = req.app.get('log');

  pub.send(['channel1', 'test message']);
  pub.send(['channel2', 'not your message']);
  
  res.render('index', { title: 'Express' });
});

module.exports = router;
